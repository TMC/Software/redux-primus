const open = (dispatch, primus) => () => dispatch({ type: 'PRIMUS_OPEN', data: { socketId: primus.socket.id } });
const end = dispatch => () => dispatch({ type: 'PRIMUS_END' });
const error = dispatch => error => dispatch({ type: 'PRIMUS_ERROR', data: error });
const reconnect = dispatch => opts => dispatch({ type: 'PRIMUS_RECONNECT', data: opts });
const reconnectScheduled = dispatch => opts => dispatch({ type: 'PRIMUS_SCHEDULED', data: opts });
const reconnected = dispatch => opts => dispatch({ type: 'PRIMUS_RECONNECTED', data: opts });
const reconnectTimeout = dispatch => (error, opts) => dispatch({ type: 'PRIMUS_TIMEOUT', data: { error, opts } });
const reconnectFailed = dispatch => (error, opts) => dispatch({ type: 'PRIMUS_FAILED', data: { error, opts } });

const connectPrimusSignals = (primus, dispatch) => {
  primus.on('open', open(dispatch, primus));
  primus.on('end', end(dispatch));
  primus.on('error', error(dispatch));
  primus.on('reconnect', reconnect(dispatch));
  primus.on('reconnect scheduled', reconnectScheduled(dispatch));
  primus.on('reconnected', reconnected(dispatch));
  primus.on('reconnect timeout', reconnectTimeout(dispatch));
  primus.on('reconnect failed', reconnectFailed(dispatch));
};

export default function createPrimusMiddleware(spark, criteria = [],
  { eventName = 'action', execute = defaultExecute } = {}) {
  const emitBound = spark.send.bind(spark);

  return ({ dispatch }) => {
    // Wire socket.io to dispatch actions sent by the server.
    connectPrimusSignals(spark, dispatch);
    spark.on(eventName, dispatch);
    return next => (action) => {
      if (evaluate(action, criteria)) {
        return execute(action, emitBound, next, dispatch);
      }
      return next(action);
    };
  };

  function evaluate(action, option) {
    if (!action || !action.type) {
      return false;
    }

    const { type } = action;
    let matched = false;
    if (typeof option === 'function') {
      // Test function
      matched = option(type, action);
    } else if (typeof option === 'string') {
      // String prefix
      matched = type.indexOf(option) === 0;
    } else if (Array.isArray(option)) {
      // Array of types
      matched = option.some(item => type.indexOf(item) === 0);
    }
    return matched;
  }

  function defaultExecute(action, emit, next, dispatch) { // eslint-disable-line no-unused-vars
    emit(eventName, action);
    return next(action);
  }
};
