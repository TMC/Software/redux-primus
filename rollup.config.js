import babel from 'rollup-plugin-babel';
import eslint from 'rollup-plugin-eslint';
import uglify from 'rollup-plugin-uglify';
import { minify } from 'uglify-js-harmony';
const _ = require('lodash');

const DEBUG = !(process.env.BUILD === 'production');
let common = {
  entry: 'src/index.js',
  targets: [
    { dest: 'dist/index.cfs.js', format: 'cjs' },
    { dest: 'dist/index.es.js', format: 'es' }
  ],
  plugins: [
  ]
};

let development = {
  sourceMap: true,
  plugins: [
    eslint(),
    babel({
      exclude: 'node_modules/**' // only transpile our source code
    })
  ]
};

let production = {
  sourceMap: true,
  plugins: [
    eslint({ throwError: true }),
    babel({
      exclude: 'node_modules/**' // only transpile our source code
    }),
    uglify({}, minify)
  ]
};
const customizer = (objValue, srcValue) => {
  if (_.isArray(objValue)) return objValue.concat(srcValue);
};
const config = DEBUG ? _.mergeWith(common, development, customizer) : _.mergeWith(common, production, customizer);

export default config;
